This repository contains the configs, code and samples used to evaluate the new python based overlap correction for LARA2

Used reanalysis settings: 
Cosmic Ray Removal - ON
Astigmasim Correction - ON
Background Removal - ON 
Overlap Correction - ON, peak width changed to 14
Peak Fitting - ON

All others OFF, standard settings used if not specified otherwise