"""
Übersetzung der bestehenden Larasoft2 Analysefunktionen von Labview zu Python 3.6,
mit dem Ziel der besseren Lesbarkeit und Instandhaltung.


"""
#Autor: Felix Schwarz
#Stand: 04.12.2023
import numpy as np
from scipy.signal import savgol_filter
from scipy.optimize import curve_fit


def shot_noise_calc(spectrumIn):
    """Shotnoise Berechnung.

    Berechnet die Intensität aus der Summe aller Komponenten des Spektrumarrays und das Schrotrauschen aus einem vorher aufgespaltenem Spektrum, indem die Wurzel der absoluten Intensität gebildet wird.

    Parameters
    ----------
    spectrumIn: 2D float Array
        Input spectrum, typischerweise 1400x20

    Returns
    -------
    intensities: float
        Intensität des Spektrums
    shot_noise: float
        Schrotrauschen des Spektrums

    """
    if not spectrumIn:
        raise Exception("Error in shot_noise_calc: Input spectrum is empty!")
    spectrumIn = np.array(spectrumIn)
    intensities = np.sum(spectrumIn)
    shot_noise = np.sqrt(np.abs(intensities))
    return intensities, shot_noise


def spectral_sensitivity_uncertainty(residueSCARF, specSensUncertainty, specSens, SCARFCluster, medianBaseline,
                                     useSCARF):
    """Berechnet Zwischenergebnisse zur Berechnung der Unsicherheit der spectral sensitivity correction. Die weitere Berechnung wird in Labview durchgeführt.

    Parameters
    ----------
    residueSCARF: 2D float array
        Residuen aus dem SCARF
    specSensUncertainty: 2D float array
        Unsicherheit der spektralen Sensitivitätsfaktoren
    specSens: 2D float array
        Spektrale Sensitivitätsfaktoren
    SCARFCluster: Cluster aus 8 Elementen
        Cluster mit Einstellungen des SCARF, näher beschreiben in Background_removal_2D
    medianBaseline: Cluster aus 3 Elementen
        Cluster mit Einstellungen der MedianBaseline, näher beschreiben in Background_removal_2D
    useSCARF: Boolean
        Wahr, falls SCARF verwendet werden soll, sonst wird ein rolling circle filter angewendet
    Returns
    -------
    summedSpectrum1: 1D float Array 
    summedSpectrum2: 1D float Array 
    upperCombined: 2D float Array
    lowerCombined: 2D float Array
    comparison: 1D float Array
    comparison2: 1D float Array
    waveformGraph5: 1D float Array
    waveformGraph4: 1D float Array
    numeric1: int
    numeric2: int
    numeric3: int

    """

    specSens = np.array(specSens)
    index = int(np.size(specSens, 0) / 2)
    waveformGraph5 = np.append(specSens[index - 1], (specSens[index]))
    upperArray = (specSens[index - 1] - 1)[::-1]
    lowerArray = (specSens[index] - 1)[::-1]
    waveformGraph4 = np.append(upperArray, lowerArray)

    sizeUpper = np.size(upperArray, 0)
    upperArray = np.append(upperArray, 0.0)  # in labview wird hier auf Standardwerte zurückgegriffen, falls
    # feldindizes außerhalb des definierten bereichs liegen. Bei double array standardmäßig: 0.0
    sizeLower = np.size(lowerArray, 0)
    lowerArray = np.append(lowerArray, 0.0)
    numeric1 = 0
    while True:
        bool1 = numeric1 > sizeUpper
        bool2 = np.sign(upperArray[numeric1]) != np.sign(upperArray[numeric1 + 1])
        if bool1 or bool2:
            break
        numeric1 += 1
        # hier neu schreiben, loop unten ist nicht korrekt.
    numeric2 = 0
    while True:
        bool1 = numeric2 > sizeLower
        bool2 = np.sign(lowerArray[numeric2]) != np.sign(lowerArray[numeric2 + 1])
        if bool1 or bool2:
            break
        numeric2 += 1

    numeric3 = int((numeric1 + numeric2) / 2)
    splitIndex = sizeUpper - numeric3
    upperCombined = np.zeros_like(specSens)
    lowerCombined = np.zeros_like(specSens)
    for i in range(len(specSens)):
        specSensUpperArray = specSens[i][:splitIndex]
        specSensLowerArray = specSens[i][splitIndex:]
        specSensUncUpperArray = specSensUncertainty[i][:splitIndex]
        specSensUncLowerArray = specSensUncertainty[i][splitIndex:]

        upperCombined[i] = np.append((specSensUpperArray + specSensUncUpperArray), (
                specSensLowerArray - specSensUncLowerArray))
        lowerCombined[i] = np.append((specSensUpperArray - specSensUncUpperArray), (
                specSensLowerArray + specSensUncLowerArray))

    upperCombined = np.absolute(upperCombined)
    lowerCombined = np.absolute(lowerCombined)
    upperCombined = np.where(upperCombined <= 0.1, 0.1, upperCombined)
    lowerCombined = np.where(lowerCombined <= 0.1, 0.1, lowerCombined)
    comparison = np.append(upperCombined[index], (lowerCombined[index]))

    _, _, _, _, correctedSpectrum1 = spectral_sensitivity_correction(residueSCARF, upperCombined)

    _, _, _, _, correctedSpectrum2 = spectral_sensitivity_correction(residueSCARF, lowerCombined)
    comparison2 = np.append(correctedSpectrum2[index],correctedSpectrum1[index])


    summedSpectrum1 = sum_spectrum(correctedSpectrum1)
    summedSpectrum2 = sum_spectrum(correctedSpectrum2)

    if useSCARF:
        _, summedSpectrum1, _ = Background_removal_1D(summedSpectrum1, SCARFCluster, medianBaseline)
        _, summedSpectrum2, _ = Background_removal_1D(summedSpectrum2, SCARFCluster, medianBaseline)


    return summedSpectrum1, summedSpectrum2, upperCombined, lowerCombined, comparison, comparison2, \
        waveformGraph5, waveformGraph4, numeric1, numeric2, numeric3


def gaussian_fit_least_squares(x, y, weight, tolerance, initialGuess,
                               bounds=None):
    """Gausscher least squares fit.

    Führt einen Gauss Fit mit der least squares Methode durch. Falls die Grenzen oben und unten gleich sind, werden die Werte der Gaussfunktion mit als Parameter eingesetzte Grenzen zurückgegeben.

    Parameters
    ----------
    x: 1D float Array
        x-Werte des Spektrums
    y: 1D float Array
        y-Werte des Spektrums
    weight: 1D float array
        Gewichtung der Werte des Spektrums
    tolerance: float
        Toleranz der Parameteranpassung
    initialGuess: 1D float Array
        Erste Vermutung der Parameteranpassung
    bounds: 1D float Array
        Grenzen der Paramateranpassung

    Returns
    -------
    bestFit: 1D float Array
        y-Werte der besten Anpassung
    amplitude: float
        Amplitude der Gausskurve der besten Anpassung
    center: float
        Zentrum der Gausskurve der besten Anpassung
    standardDeviation: float
        Standardabweichung der Gausskurve der besten Anpassung
    residues: 1D float Array
        Residuen
    offset: float
        y-Achsenabschnitt der Gausskurve der besten Anpassung

    """


    def gauss_function(x, a, x0, sigma, c):
        return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2)) + c

    if np.array_equal(bounds, [0, 0, 0, 0, 0, 0, 0, 0]):
        bounds = [0, np.inf, 0, np.inf, 0, np.inf, -np.inf, np.inf]
    lowerbounds = [bounds[0], bounds[2], bounds[4], bounds[6]]
    upperbounds = [bounds[1], bounds[3], bounds[5], bounds[7]]
    y = np.array(y)
    x = np.array(x)
    weight = np.array(weight)
    if np.array_equal(weight, np.zeros(len(y))) or np.array_equal(weight, []):
        weight = np.ones(len(y))

    if (bounds[0] == bounds[1]) & (bounds[2] == bounds[3]) & (bounds[4] == bounds[5]):
        bestfit = gauss_function(x, bounds[0], bounds[2], bounds[4], bounds[6])
        residues = np.sum(weight * (bestfit - y) ** 2)
        return bestfit, bounds[0], bounds[2], bounds[4], residues, 0

    popt, pcov = curve_fit(gauss_function, x, y, p0=initialGuess,
                           bounds=(lowerbounds, upperbounds), maxfev=30000)  # benutzt nicht lineare least squares
    bestFit = np.array(gauss_function(x, *popt))

    amplitude, center, standardDeviation, offset = popt

    residues = np.sum(weight * (bestFit - y) ** 2)
    return bestFit, amplitude, center, standardDeviation, residues, 0


"""def gaussian_fit_least_absolute_residue(x, y, weight, tolerance, initialGuess, bounds):
    bestFit, amplitude, center, standardDeviation, residue =0,0,0,0,0
    plt.plot(x, y)
    plt.plot(x, bestFit)
# https://stackoverflow.com/questions/51883058/l1-norm-instead-of-l2-norm-for-cost-function-in-regression-model
    return bestFit, amplitude, center, standardDeviation, residue


def gaussian_fit_quartic(x, y, weight, tolerance, initialGuess, bounds):
    bestFit = Polynomial.fit(x, y, deg=4)  # biquadratischer fit. nvm es ist wahrscheinlich ein anderer gemeint
    # keine direkte implementierung für bisquare interpolation gefunden, gewichte selbst berechnen?
    # ist bigauss gemeint?
    bestFit, amplitude, center, standardDeviation, residue = 0,0,0,0,0
    return bestFit, amplitude, center, standardDeviation, residue
"""


def gaussian_peak_fit(x, y, weight, tolerance, initialGuess, bounds):
    """Gausscher peak fit.

    Führt einen Gauss Fit durch und gibt einen Fehler zurück, falls er fehlschlägt.

    Parameters
    ----------
    x: 1D float Array
        x-Werte des Spektrums
    y: 1D float Array
        y-Werte des Spektrums
    weight: 1D float array
        Gewichtung der Werte des Spektrums
    tolerance: float
        Toleranz der Parameteranpassung
    initialGuess: 1D float Array
        Erste Vermutung der Parameteranpassung
    bounds: 1D float Array
        Grenzen der Paramateranpassung

    Returns
    -------
    bestFit: 1D float Array
        y-Werte der besten Anpassung
    amplitude: float
        Amplitude der Gausskurve der besten Anpassung
    center: float
        Zentrum der Gausskurve der besten Anpassung
    standardDeviation: float
        Standardabweichung der Gausskurve der besten Anpassung
    residues: 1D float Array
        Residuen
    offset: float
        y-Achsenabschnitt der Gausskurve der besten Anpassung
    error: int
        Fehlermeldungen
    """
    if not x:
        raise Exception("Error in gaussian_peak_fit: x input is empty!")
    if not y:
        raise Exception("Error in gaussian_peak_fit: y input is empty!")
    error = 0
    bestFit, amplitude, center, standardDeviation, residue, offset = gaussian_fit_least_squares(x, y, weight, tolerance,
                                                                                                initialGuess, bounds)

    return bestFit, amplitude, center, standardDeviation, offset, residue, error


def Background_removal_2D(inputSpectrum, ScarfCluster, MedianBaseline):
    """2D Hintergrundentfernung.

    Führt eine 2D Hintergrundentfernung durch, indem die 1D Hintergrundentfernung für jede Zeile
    des Spektrumarrays aufgerufen wird.

    Parameters
    ----------
    inputSpectrum: 2D float Array
        Input spectrum, typischerweise 1400x20
    ScarfCluster: Cluster aus 8 Elementen
        Cluster mit Einstellungen des SCARF. Enthält
            rollingCircleVersion: Bool
                SCARF falls wahr, RCF sonst
            numberOfIterations: Bool
                Zwei Iterationen falls wahr, sonst eine
            constantBaseline: float
                Höhe der Grundlinie
            sgPolynomialOrder: int
                Polynomordnung, welche beim Savitzky-Golay Filter angewendet wird
            rcfRadius1: float
                Radius des rollenden Kreises der ersten Iteration
            sgSidepoints1: int
                Anzahl der Seitenpunkte, welche beim Savitzky-Golay Filter der ersten Iteration angewendet werden
            rcfRadius2: float
                Radius des rollenden Kreises der zweiten Iteration
            sgSidepoints2: int
                Anzahl der Seitenpunkte, welche beim Savitzky-Golay Filter der zweiten Iteration angewendet werden
    MedianBaseline: Cluster aus 3 Elementen.
        Enthält
            use: Bool
                Verwendet die MedianBaseline falls wahr, sonst nicht
            minRange: float
                Untere Grenze der Baseline
            maxRange: float
                Obere Grenze der Baseline
    Returns
    -------
    residueBeforeMedianBaselineSubtraction: 2D float Array
        Residuen vor Abzug der MedianBaseline
    residue: 2D float Array
        Residuen
    locus: 2D float Array
        Locus
    """
    if not inputSpectrum:
        raise Exception("Error in Background_removal_2D: Input spectrum is empty!")
    if not ScarfCluster:
        raise Exception("Error in Background_removal_2D: Scarf cluster is empty!")
    if not MedianBaseline:
        raise Exception("Error in Background_removal_2D: MedianBaseline cluster is empty!")


    array = np.array([list(Background_removal_1D(inputS, ScarfCluster, MedianBaseline)) for inputS in inputSpectrum])
    residueListBeforeMBSubstraction = array[:, 0]
    residueList = array[:, 1]
    locus = array[:, 2]

    return residueList.tolist(), residueListBeforeMBSubstraction.tolist(), locus.tolist()


def Background_removal_1D(inputSpectrum, ScarfCluster, MedianBaseline):
    """1D Hintergrundentfernung.

    Führt mittels SCARF oder RCF Methode eine Hintergrundentfernung durch.

    Parameters
    ----------

    inputSpectrum: 1D float Array
        Input spectrum
    ScarfCluster: Cluster aus 8 Elementen
        Cluster mit Einstellungen des SCARF, näher beschrieben in Background_removal_2D
    MedianBaseline: Cluster aus 3 Elementen
        Cluster mit Einstellungen der MedianBaseline, näher beschreiben in Background_removal_2D

    Returns
    -------
    residueBeforeMedianBaselineSubtraction: 1D float Array
        Residuen vor Abzug der MeadianBaseline
    residue: 1D float Array
        Residuen
    locus: 1D float Array
        Locus

    """

    if ScarfCluster[0]:
        if ScarfCluster[1]:
            bypassedSpectrum1, locus1, residue1, RCFlocus1 = SCARF(inputSpectrum, ScarfCluster[4], 0, ScarfCluster[3],
                                                                   ScarfCluster[5])
            # RCF radius 1, baseline = 0, polynomial order , sg-sidepoints 1
            bypassedSpectrum2, locus2, residue2, RCFlocus2 = SCARF(residue1, ScarfCluster[6], ScarfCluster[2],
                                                                   ScarfCluster[3], ScarfCluster[7])
            # inputs: residue 1, RCF radius2 , baseline, polynomial order, sg-sidepoints2
            locus = locus1 + locus2
            residueBeforeMBSubstraction = residue2

        else:
            bypassedSpectrum, locus, residueBeforeMBSubstraction, RCFlocus = SCARF(inputSpectrum, ScarfCluster[4],
                                                                                   ScarfCluster[2], ScarfCluster[3],
                                                                                   ScarfCluster[5])
            # inputs: RCF radius 1, baseline, polynomial order , sg-sidepoints 1
    else:
        bypassedSpectrum, locus, residue1, RCFlocus = SCARF(inputSpectrum, ScarfCluster[4], ScarfCluster[2],
                                                            ScarfCluster[3], ScarfCluster[5])
        # inputs: RCF radius 1, baseline, polynomial order , sg-sidepoints 1
        locus = RCFlocus
        residueBeforeMBSubstraction = bypassedSpectrum - locus

    if MedianBaseline[0]:

        mean = np.mean(residueBeforeMBSubstraction[int(MedianBaseline[1]):int(MedianBaseline[2])])
        residue = residueBeforeMBSubstraction - mean
    else:
        residue = residueBeforeMBSubstraction
    return residueBeforeMBSubstraction, residue, locus


def SCARF(inputSpectrum, RCFradius, baseline, polynomialOrder, sg_sidepoints):
    """

    Führt den Savitzky-Golay Coupled Advanced Rolling Circle Filter (SCARF) durch.

    Parameters
    ----------
    inputSpectrum: 2D float Array
        Input Spektrum, typischerweise 1400x20
    RCFradius: int
        Radius des rollenden Kreises
    baseline: double
        Grundlinie
    polynomialOrder: int
        Polynomordnung, welche beim Savitzky-Golay Filter angewendet wird
    sg_sidepoints: int
        Anzahl der Seitenpunkte, welche beim Savitzky-Golay Filter angewendet werden
    Returns
    -------
    bypassedSpectrum: 2D float Array
        Input Spektrum
    locus: 1D float Array
        Locus
    residue: 1D float Array
        Residuen
    RFClocus: 1D float Array
        Locus des RFC, also ohne Savitzky-Golay-Filter und Grundlinie

    """
    inputSpectrum = np.array(inputSpectrum)
    RCFradius = int(RCFradius)
    polynomialOrder = int(polynomialOrder)
    sg_sidepoints = int(sg_sidepoints)

    size1 = (2 * RCFradius + 1)
    size2 = len(inputSpectrum) - (RCFradius + 1) * 2
    size3 = RCFradius + 1

    mina, minc, minb = np.zeros(size3), np.zeros(size3), np.zeros(size2)
    semicircle = np.array([np.sqrt(RCFradius ** 2 - (i - RCFradius) ** 2) - RCFradius for i in range(size1)])

    # case b (r+1) >= i >= (N-(r+1))
    minb = np.array([np.min(inputSpectrum[i + 1:i + 1 + size1] - semicircle) for i in range(size2)])
    # for i in range(size2):
    #    minb[i] = np.min(inputSpectrum[i + 1:i + 1 + size1] - semicircle)

    # case a i < (r+1)

    for i in range(size3):  # optimierungsbedarf
        placeholderSpectruma1 = inputSpectrum[0:RCFradius * 2]
        placeholderSpectruma2 = semicircle[1:1 + RCFradius * 2]
        placeholderSpectruma2 = placeholderSpectruma2[RCFradius - i:]
        placeholderSpectruma1 = placeholderSpectruma1[0:len(placeholderSpectruma2)]
        mina[i] = np.min(placeholderSpectruma1 - placeholderSpectruma2)

    # case c i > (N-(r+1))

    for i in range(size3):
        placeholderSpectrumc1 = inputSpectrum[::-1]
        placeholderSpectrumc1 = placeholderSpectrumc1[0:RCFradius * 2]
        placeholderSpectrumc2 = semicircle[1:1 + RCFradius * 2]
        placeholderSpectrumc2 = placeholderSpectrumc2[RCFradius - i:]
        placeholderSpectrumc1 = placeholderSpectrumc1[0:len(placeholderSpectrumc2)]

        minc[i] = np.min(placeholderSpectrumc1 - placeholderSpectrumc2)

    minc = minc[::-1]
    mins = mina

    mins = np.concatenate((mins, minb, minc))

    RFClocus = mins
    bypassedSpectrum = inputSpectrum

    locus = savgol_filter(mins, 2 * sg_sidepoints + 1, polynomialOrder, mode='interp') + baseline

    residue = bypassedSpectrum - locus
    return bypassedSpectrum, locus, residue, RFClocus


def cosmic_ray_removal(currentSpectrum, previousSpectrum, threshold):
    """Entfernung kosmischer Hintergrundstrahlung.

    Entfernung der kosmischen Hintergrundstrahlung aus dem Spektrum.
    liegt die absolute Differenz eines Punktes beider Spektren über einem gegebenem
    positivem threshold, wird der Punkt mit dem kleineren Wert in das neue Spektrum eingesetzt.
    Liegt die Differenz nicht über dem treshold,
    wird der Mittelwert des Punktes beider Spektren gebildet und eingesetzt.
    Implementiert nach 'Automated Quantitative Spectroscopic Analysis Combining
    Background Subtraction, Cosmic Ray Removal, and Peak Fitting' (James, 2019)
    previousspectrum und currentspectrum sind 2D-Arrays in denen die Intensität abgespeichert wird.

    Parameters
    ----------
    currentSpectrum: 2D float Array
        Aktuelles Input spectrum, typischerweise 1400x20
    previousSpectrum: 2D float Array
        Vorheriges Input spectrum, typischerweise 1400x20
    threshold: double
        Schwelle, ab der die Differenz eines Pixels beider Spektren als komische Strahlung gewertet wird

    Returns
    -------
    correctedSpectrum: 2D float Array
        Um kosmische Strahlung korrigiertes Spektrum
    numberOfCosmics: int
        Anzahl der korrigierten kosmischen Strahlen

    """

    if not currentSpectrum:
        raise Exception("Error in cosmic_ray_removal: Current spectrum is empty!")


    previousSpectrum = np.array(previousSpectrum)
    currentSpectrum = np.array(currentSpectrum)

    if not (previousSpectrum.shape == currentSpectrum.shape):
        if len(previousSpectrum) != 0:  # bedingung um allererstes previousspectrum zu beachten
            raise Exception("Error in cosmic_ray_removal: Current and previous spectrum do not have the same dimensions!")  # Haben die Spektren nicht die gleiche Größe, wird hier abgebrochen.
        else: # für das allererste Spektrum wird keine Korrektur durchgeführt.
            return currentSpectrum, 0
    diff = np.abs(currentSpectrum - previousSpectrum)
    numRows = len(diff)
    numCollums = len(diff[0])

    correctedSpectrum1 = np.array([[(previousSpectrum[i][j] + currentSpectrum[i][j]) / 2.000 if diff[i][j] < threshold
                                    else (min(previousSpectrum[i][j], currentSpectrum[i][j]))
                                    for j in range(numCollums)] for i in range(numRows)])

    # correctedSpectrum1 = np.where(diff < threshold, (previousSpectrum - currentSpectrum) / 2.0, np.minimum(previousSpectrum, currentSpectrum)) #ist wohl nicht äquivalent
    numberOfCosmics1 = np.sum(diff >= threshold)
    correctedSpectrum1 = correctedSpectrum1.tolist()
    return correctedSpectrum1, numberOfCosmics1


def dead_pixel_removal(inputSpectrum, deadPixelCoords):
    """Entfernung totet Pixel.

    Entfernung toter Pixel aus dem Spektrum.
    Die Koordinaten des toten Pixels werden vom Nutzer geliefert.
    Hierbei wird ein 2D-Array mit den Koordinaten der toten Pixel übergeben (floats).
    Der Wert eines toten Pixels wird mit dem Wert seines linken Nachbars (x-1) ersetzt.
    inputspectrum ist ein 2D-Array.

    Parameters
    ----------
    inputSpectrum: 2D float Array
        Input spectrum, typischerweise 1400x20
    deadPixelCoords: 2D int Array
        Koordinaten der toten Pixel

    Returns
    -------
    correctedSpectrum: 2D float Array
        Um tote Pixel korrigiertes Spektrum

    """
    if not inputSpectrum:
        raise Exception("Error in dead_pixel_removal: Input spectrum is empty!")
    correctedspectrum = np.asarray(inputSpectrum)
    inputSpectrum = np.asarray(inputSpectrum)

    for i in range(int(len(deadPixelCoords))):
        xdead, ydead = int(deadPixelCoords[i][0]), int(deadPixelCoords[i][1])

        if xdead < inputSpectrum.shape[1] and 0 <= ydead < inputSpectrum.shape[0]:
            if xdead > 0:
                correctedspectrum[ydead][xdead] = inputSpectrum[ydead][xdead - 1]
            if xdead == 0:
                correctedspectrum[ydead][xdead] = inputSpectrum[ydead][xdead + 1]

    return correctedspectrum


def region_of_interest(inputSpectrum, roi):
    """Bildet einen ROI-Ausschnitt aus inputSpectrum.

    Wendet die vom Anwender spezifizierte ROI auf das Spektrum an.
    Das 1D-Inputspektrum wird hier nicht bearbeitet,
    da es in der Labview Version nicht angenommen oder ausgegeben wird.
    roi besteht aus den Grenzen der ROI, dabei ist der erste Wert einer Zeile die untere Grenze.
    Erste Zeile x-Grenzen, zweite Zeile y-Grenzen.
    Wir geben also das Array vom minimalen Index bis zum maximalen Index zurück.
    Das inputSpectrum Array besteht aus 20 Zeilen (den y-Gruppen), mit 1400 Spalten (den x-Werten).

    Parameters
    ----------
    inputSpectrum: 2D float Array
        Input spectrum, typischerweise 1400x20
    roi: 2D int Array
        Koordinaten der region of interest

    Returns
    -------
    roiSpectrum: 2D float Array
        Region of interest-Ausschnitt des Spektrums

    """
    if not inputSpectrum:
        raise Exception("Error in region_of_interest: Input spectrum is empty!")

    inputSpectrum = np.array(inputSpectrum)
    roi = np.array(roi)  # In roi sind die ROI Grenzen gespeichert

    if roi[0][0] < 0 or roi[0][1] > inputSpectrum.shape[1] or roi[1][0] < 0 or roi[1][1] > inputSpectrum.shape[0]:
        return inputSpectrum  # Sind die ROI Grenzen außerhalb der Reichweite, wird hier abgebrochen.

    roiSpectrum = inputSpectrum[roi[1][0]:roi[1][1] + 1, roi[0][0]:roi[0][1] + 1]  # endindex ist nicht inklusive
    roiSpectrum = roiSpectrum.tolist()

    # Labview akzeptiert nur Listen, keine numpy Arrays
    return roiSpectrum


def test():
    testarrayx = [[0, 1, 2, 3, 4, 5, 6], [0, 1, 3, 4, 5, 6, 7], [0, 1, 3, 4, 5, 6, 8]]
    testarrayy = [[0, 10, 20, 30, 40, 50, 200], [0, 10, 30, 40, 50, 60, 80], [0, 10, 30, 40, 50, 60, 80]]
    correctedarray = np.zeros(shape=(len(testarrayx), len(testarrayy[0])))
    for i in range(len(testarrayy)):
        correctedarray[i] = np.interp([0, 1, 2, 3, 4, 5, 7], testarrayx[i], testarrayy[i])
    print(correctedarray)


def astigmatism_correction(inputSpectrum, astigmatismArray):
    """Korrigiert Astigmatismus.

    Falls bei Arrays die gleiche Größe haben, werden die y-Werte an den Stellen x des astigmatismArrays
    über lineare Interpolation (über eine !Spalte!) des inputSpectrums ermittelt.

    Parameters
    ----------
    inputSpectrum: 2D float Array
        Input spectrum, typischerweise 1400x20
    astigmatismArray: 2D float Array
        2D Array mit Astigmatismuskorrekturfaktoren

    Returns
    -------
    correctedSpectrum: 2D float Array
        Astigmatismus korrigiertes Spektrum
    isSameSize: Bool
        Wahr, falls die Parameter die gleiche Dimension haben
    inputSpectrumSize: 1D int Array
        Dimensionen des inputSpectrum
    astigmatismArraySize: 1D int Array
        Dimensionen des astigmatismArray

    """
    inputSpectrumSize = np.shape(inputSpectrum)
    astigmatismArraySize = np.shape(astigmatismArray)

    if inputSpectrumSize[0] == astigmatismArraySize[1] and inputSpectrumSize[1] == astigmatismArraySize[0]:
        xarray = np.arange(inputSpectrumSize[0])
        inputSpectrum = np.transpose(inputSpectrum)

        correctedSpectrum = np.array(
            [np.interp(astigmatismArray[i], xarray, inputSpectrum[i]) for i in range(inputSpectrumSize[1])])

        correctedSpectrum = np.transpose(correctedSpectrum)
        correctedSpectrum = correctedSpectrum.tolist()
        inputSpectrumSize = list(inputSpectrumSize)
        astigmatismArraySize = list(astigmatismArraySize)

        return correctedSpectrum, True, inputSpectrumSize, astigmatismArraySize

    else:
        raise Exception("Error in astigmatism_correction: Input and astigmatism array do not have the same dimensions!")
        #inputSpectrumSize = list(inputSpectrumSize)
        #astigmatismArraySize = list(astigmatismArraySize)
        # Haben die Spektren nicht die gleiche Größe, wird hier abgebrochen.

        #return inputSpectrum, False, inputSpectrumSize, astigmatismArraySize


def spectral_sensitivity_correction(inputSpectrum, specSensitivityArray):
    """Korrigiert um spektrale Sensitivität.

    Korrigiert das eingehende Spektrum um einen spektralen Sensitivitätsfaktor.

    Parameters
    ----------
    inputSpectrum: 2D float Array
        Input spectrum, typischerweise 1400x20
    specSensitivityArray: 2D float Array
        2D Array mit spektralen Sensitivitätskorrekturfaktoren
    Returns
    -------
    specSensitivityArray: 2D float Array
        2D Array mit spektralen Sensitivitätskorrekturfaktoren
    isSameSize: Bool
        Wahr, falls die Parameter die gleiche Dimension haben
    inputSpectrumSize: 1D int Array
        Dimensionen des inputSpectrum
    sccCorrectionSize: 1D int Array
        Dimensionen des specSensitivityArray
    correctedSpectrum: 2D float Array
        Um spektrale Sensitivität korrigiertes Spektrum

    """

    if not inputSpectrum:
        raise Exception("Error in spectral_sensitivity_correction: Input spectrum is empty!")
    inputSpectrumSize = np.shape(inputSpectrum)
    sccCorrectionSize = np.shape(specSensitivityArray)
    sccCorrectionSize = list(sccCorrectionSize)
    inputSpectrumSize = list(inputSpectrumSize)
    if not (inputSpectrumSize == sccCorrectionSize):
        raise Exception("Error in spectral_sensitivity_correction: Input and specsens array do not have the same dimension!")
        #return specSensitivityArray, False, inputSpectrumSize, sccCorrectionSize, inputSpectrum
    else:
        correctedSpectrum = np.divide(inputSpectrum, specSensitivityArray)
        correctedSpectrum = correctedSpectrum.tolist()
        return specSensitivityArray, True, inputSpectrumSize, sccCorrectionSize, correctedSpectrum


def sum_spectrum(inputSpectrum):
    """Summiert das Spektrum.

    Wird in der Labview version unter 'Binning' aufgeführt.
    Das inputSpectrum wird entlang der x-Achse summiert.

    Parameters
    ----------
    inputSpectrum: 2D float Array
        Input spectrum, typischerweise 1400x20

    Returns
    -------
    spectrumSum: 1D float Array
        Entlang der x-Achse summiertes Spektrum

    """

    spectrumSum = np.sum(inputSpectrum, axis=0)
    return spectrumSum


def calculation_concentration_conversion_uncertainty(consideredPeaks, theoCorrUncertainty, intensities,
                                                     calibrationUncertainty, theoCorr):
    """

    Berechnet die korrigierte Intensität und deren Unsicherheit, die relative Konzentration und deren Unsicherheit.

    Parameters
    ----------
    consideredPeaks: 1D Boolean Array
        Gibt an, welche der Gase berücksichtigt werden.
    theoCorrUncertainty: 1D float Array
        Enthält die Unsicherheit der theoretischen Korrekturwerte
    intensities: 1D float Array
        Enthält die Intensitäten des Spektrum
    calibrationUncertainty: 1D float Array
        Enthält die Unsicherheiten der Kalibrierung
    theoCorr: 1D float Array
        Enthält die theoretischen Korrekturwerte

    Returns
    -------
    relConcentrations: 1D float Array
        Enthält die relativen Konzentrationen der berücksichtigten Gase
    corrIntensities: 1D float Array
        Enthält die korrigierten Intensitäten des Spektrums
    relConcentrationUncertainties: 1D float Array
        Enthält die relativen Konzentrationsunsicherheiten der berücksichtigten Gase
    totalCalibrationUncertainty: 1D float Array
        Enthält die absolute Kalibrationsunsicherheit der berücksichtigten Gase

    """

    theoCorr = np.array(theoCorr)
    theoCorrUncertainty = np.array(theoCorrUncertainty)
    intensities = np.array(intensities)
    calibrationUncertainty = np.array(calibrationUncertainty)
    calibration_size = len(calibrationUncertainty)
    if len(calibrationUncertainty) > len(consideredPeaks):
        totalCalibrationUncertainty = np.zeros(len(consideredPeaks))
        corrIntensities = np.zeros(len(consideredPeaks))
    else:
        totalCalibrationUncertainty = np.zeros(calibration_size)
        corrIntensities = np.zeros(calibration_size)

    for i in range(calibration_size):
        totalCalibrationUncertainty[i] = np.sqrt(
            (theoCorrUncertainty[i] * intensities[i]) ** 2 + (calibrationUncertainty[i] * theoCorr[i]) ** 2)
        corrIntensities[i] = intensities[i] * theoCorr[i]

    numeric = 0
    for i in range(calibration_size):
        if consideredPeaks[i]:
            numeric = numeric + corrIntensities[i]
    relConcentrations = np.zeros(calibration_size)

    for i in range(calibration_size):
        if consideredPeaks[i]:
            relConcentrations[i] = corrIntensities[i] / numeric
        else:
            relConcentrations[i] = 0

    relConcentrationUncertainties = np.zeros(calibration_size)
    for i in range(calibration_size):
        placeholdervalue = np.zeros(calibration_size)
        if consideredPeaks[i]:
            for j in range(len(totalCalibrationUncertainty)):
                if j == i:
                    placeholdervalue[j] = (((numeric - corrIntensities[i]) / (
                            numeric ** 2)) * totalCalibrationUncertainty[j]) ** 2

                else:
                    placeholdervalue[j] = ((totalCalibrationUncertainty[j] * corrIntensities[i]) / (numeric ** 2)) ** 2

            relConcentrationUncertainties[i] = np.sqrt(np.sum(placeholdervalue))
        else:
            relConcentrationUncertainties[i] = 0
    # die letzten beiden loops könnte man vereinen, zur einfacheren zuordnung mach' ich das noch nicht

    return relConcentrations, corrIntensities, relConcentrationUncertainties, totalCalibrationUncertainty


def calculation_relative_intensities_uncertainties(consideredPeaks, ramanSignalUncertainties, ramanSignal):
    """

    Berechnet die relative Konzentration, deren Unsicherheit und die Summe des Ramansignals.

    Parameters
    ----------
    consideredPeaks: 1D Boolean Array
        Gibt an, welche der Gase berücksichtigt werden.
    ramanSignalUncertainties: 1D Array
        Enthält die Unsicherheiten des Ramansignals
    ramanSignal: 1D Array
        Enthält das Ramansignal
    Returns
    -------
    relConcentrationsUncertainties: 1D float Array
        Enthält die relativen Konzentrationsunsicherheiten der berücksichtigten Gase
    relConcentrations: 1D float Array
        Enthält die relativen Konzentrationen der berücksichtigten Gase
    ramanSignalSum: float
        Enthält die Summe des Ramansignals

    """
    ramanSignal = np.array(ramanSignal)
    ramanSignalUncertainties = np.array(ramanSignalUncertainties)
    ramanSignalSum = 0
    for i in range(len(ramanSignal)):
        if consideredPeaks[i]:
            ramanSignalSum = ramanSignalSum + ramanSignal[i]

    indexholder = np.zeros(len(ramanSignal))
    for i in range(len(ramanSignal)):
        if consideredPeaks[i]:
            indexholder[i] = ramanSignal[i] / ramanSignalSum
        else:
            indexholder[i] = 0
    zeroarraysize = len(indexholder) - len(ramanSignal)
    zeroarray = np.zeros(zeroarraysize)
    relConcentrations = np.insert(indexholder, len(indexholder), zeroarray)

    placeholderarray = np.zeros(len(ramanSignal))
    placeholdervalue = np.zeros(len(ramanSignalUncertainties))
    for i in range(len(ramanSignal)):
        if consideredPeaks[i]:
            for j in range(len(ramanSignalUncertainties)):
                if i == j:
                    placeholdervalue[j] = (((ramanSignalSum - ramanSignal[i]) / (
                            ramanSignalSum ** 2)) * ramanSignalUncertainties[i]) ** 2
                else:
                    placeholdervalue[j] = ((ramanSignalUncertainties[j] * ramanSignal[i]) / (
                            ramanSignalSum ** 2)) ** 2
            placeholderarray[i] = np.sqrt(np.sum(placeholdervalue))
        else:
            placeholderarray[i] = 0
    relConcentrationsUncertainties = np.insert(placeholderarray, len(indexholder), zeroarray)

    return relConcentrationsUncertainties, relConcentrations, ramanSignalSum



# file = open("C:/Users/Felix Schwarz/PycharmProjects/labview2python/Debugdatei.txt", "w+")
# file.truncate(0)
# file.close()
