import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import matplotlib.patches as mpatches

dataRMSE = pd.read_csv(r"..\results\RMSE comparison.txt",delimiter = ";", decimal =",")
data = pd.read_csv(r"..\results\values and differences.txt",delimiter = ";", decimal =",")
dataRMSE.columns=["lvRMSE","pyRMSE"]
lvRMSE = dataRMSE["lvRMSE"].to_numpy()
pyRMSE = dataRMSE["pyRMSE"].to_numpy()
x = np.arange(len(pyRMSE))
plt.scatter(x,lvRMSE,label="LabVIEW")
plt.scatter(x,pyRMSE,label="Python")
plt.ylabel("RMSD")
plt.xlabel("fitted sample")
plt.grid()
plt.legend()
plt.show()
print("python has lower RSMD by a mean factor of ",np.mean(lvRMSE/pyRMSE), "\n")

print(data)
data.columns=["method","molecule","mean","std","calib","analysis"]
mean = data["mean"].to_numpy()
calib = data["calib"].to_numpy()
analysis = data["analysis"].to_numpy()
unc = np.sqrt(analysis**2 + calib**2)
diff = np.array([abs(mean[0]-mean[3]),abs(mean[1]-mean[4]),abs(mean[2]-mean[5])])
fig, (ax1,ax2,ax3) = plt.subplots(nrows=3,ncols=1,figsize=(6,9))

ax1.errorbar(["T2","DT","HT"],y = mean[:3],yerr = unc[:3],capsize = 7,label="Python result & uncertainty",marker ="o",linestyle = "none",alpha = 0.6,color = "black")
ax1.errorbar(["T2","DT","HT"],y = mean[3:],yerr = unc[3:],capsize = 10,label="LabVIEW result & uncertainty",marker ="o",linestyle ="none",alpha = 0.6, color ="orange")
ax1.grid()
ax1.legend()

ax1.set_ylabel("mean gas\nconcentration")


ax2.errorbar(["T2\n(mean scaled down\nby a factor of 100)","DT","HT"],y = mean[:3]*[1/100,1,1],yerr = unc[:3],capsize = 10,label="Python result & uncertainty",marker ="o",linestyle = "none",alpha = 0.6,color = "black")
ax2.errorbar(["T2\n(mean scaled down\nby a factor of 100)","DT","HT"],y = mean[3:]*[1/100,1,1],yerr = unc[3:],capsize = 10,label="LabVIEW result & uncertainty",marker ="o",linestyle ="none",alpha = 0.6, color ="orange")
ax2.grid()
ax2.set_ylabel("mean gas\nconcentration")

ax3.scatter(["T2","DT","HT"],y=diff,label="difference of mean gas concentrations",marker="o")
ax3.grid()
ax3.set_ylabel("difference of \nmean gas concentrations")
ax3.set_xlabel("gas species")
ax3.sharex(ax1)
fig.tight_layout()
plt.show()

fig, (ax1,ax2,ax3) = plt.subplots(nrows=3,ncols=1,sharex="all",figsize=(6,9))

ax1.errorbar(["T2","DT","HT"],y = mean[:3],yerr = unc[:3],capsize = 10,label="Python result & uncertainty",marker ="o",linestyle = "none",alpha = 0.6,color = "black")
ax1.errorbar(["T2","DT","HT"],y = mean[3:],yerr = unc[3:],capsize = 10,label="LabVIEW result & uncertainty",marker ="o",linestyle ="none",alpha = 0.6, color ="orange")
ax1.grid()
ax1.legend()
ax1.set_ylabel("mean gas\nconcentration")


ax2.errorbar(["T2","DT","HT"],y = mean[:3]*[1/100,1,1],yerr = unc[:3],capsize = 7,label="Python result & uncertainty",marker ="o",linestyle = "none",alpha = 0.6,color = "black")
ax2.errorbar(["T2","DT","HT"],y = mean[3:]*[1/100,1,1],yerr = unc[3:],capsize = 10,label="LabVIEW result & uncertainty",marker ="o",linestyle ="none",alpha = 0.6, color ="orange")
ax2.grid()
ax2.set_ylabel("mean gas\nconcentration")
handles, labels = ax2.get_legend_handles_labels()
patch = mpatches.Patch(color='none', label='T2 mean gas concentration\nscaled down by a factor of 100')
#handles.append(patch)
handles = [patch]
ax2.legend(handles = handles)

ax3.scatter(["T2","DT","HT"],y=diff,label="difference of mean gas concentrations",marker="o")
ax3.grid()
ax3.set_ylabel("difference of \nmean gas concentrations")
ax3.set_xlabel("gas species")
fig.tight_layout()
plt.show()



#plt.scatter(["T2","DT","HT"],diff, label="difference of mean gas concentrations")
#plt.fill_between(["T2","DT","HT"],y1 = unc[3:6], label= "uncertainty given by LabVIEW", alpha = 0.4)
#plt.ylabel("gas concentration")
#plt.xlabel("gas species")
#plt.grid()
#plt.legend()
#plt.show()
#print(diff)
#print(unc[0:2], unc[2:4])
#plt.errorbar(["T2"],y=[mean[3]],yerr=[unc[3]],fmt="x",label="LabVIEW result & uncertainty",alpha = 0.6)
#plt.errorbar(["T2"], y = [mean[0]], yerr = [unc[0]],fmt="x", label="Python result & uncertainty", alpha = 0.6)
#plt.ylabel("gas concentration")
#plt.xlabel("gas species")
#plt.grid()
#plt.legend()
#plt.show()
#plt.errorbar(["DT","HT"],y=[mean[4],mean[5]],yerr=[unc[4],unc[5]],fmt="x",label="LabVIEW result & uncertainty",alpha = 0.6)
#plt.errorbar(["DT","HT"], y = [mean[1],mean[2]], yerr = [unc[1],unc[2]],fmt="x", label="Python result & uncertainty", alpha = 0.6)
#plt.ylabel("gas concentration")
#plt.xlabel("gas species")
#plt.grid()
#plt.legend()
#plt.show()
#achse mittig überspringen?